<?php
//class Usermodel extends CI_Model
if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Usermodel extends CI_Model
{
    public function getuserdata()
    {
        //     return [
        //         ['Firstname' => 'Rakesh', 'accountNo' => '978656'],
        //         ['Firstname' => 'Abhishek', 'accountNo' => '758656'],
        //     ];

        // core class in model
        $this->test(); //test function in application/core/My_model.php

        $this->load->database();
        // $this->db->select("FirstName");
        // $q = $this->db->query("SELECT * FROM userdetails");
        $this->db->where("ID", 2);
        $q = $this->db->get('userdetails'); //active class function get

        return $q->result_array();


        //for formvalidation libraries:-
        $this->load->library('form_validation');

        //adding multiple library:-
        $this->load->library(array('email', 'table'));


    }
}
?>
<html>

<head>
    <title>Users Details</title>
</head>

<body>

    <h1>User Details :- </h1>

    <table>
        <tr>
            <td>ID</td>
            <td>First Name</td>
            <td>Account No</td>

        </tr>
        <?php foreach ($users as $user): ?>
            <tr>
                <td>
                    <?php echo $user['ID']; ?>
                </td>
                <td>
                    <?php echo $user['FirstName']; ?>
                </td>
                <td>
                    <?php echo $user['AccountNo']; ?>
                </td>
            </tr>
        <?php endforeach; ?>


    </table>
    <!-- Alternate PHP Syntax for View Files -->
    <?php echo 'using echo to print'; ?>

    <!-- Alternative Echos -->
    <?php
    $var = "helo world";
    ?>
    <? //=$var?>



</body>

</html>
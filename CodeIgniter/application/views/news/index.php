<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>

    <?php foreach ($news as $news_item): ?>

        <h3 class="bg-primary">
            <?php echo $news_item['title']; ?>
        </h3>
        <div class="main">
            <?php echo $news_item['text']; ?>
        </div>
        <p><a href="<?php echo site_url('news/' . $news_item['slug']); ?>">View article</a></p>

    <?php endforeach; ?>



    <button type="button" class="btn btn-success btn-block"><a href="<?php echo site_url('news/create'); ?>">create
            News</a></button>
</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>

    <div class="container">
        <?php echo validation_errors(); ?>

        <?php echo form_open('news/create'); ?>

        <div class="form-group">
            <label for="title">News Title :</label>
            <input type="text" name="title" class="form-control" /><br />

            <label for="text">Text:</label>
            <textarea class="form-control" rows="5" name="text"></textarea><br>

            <input type="submit" name="submit" value="Create news item" class="btn btn-success" />
        </div>
        </form>
    </div>

</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>codeIgniter tutorial</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<style>
    .header {
        font-size: 30px;
        color: whitesmoke;
        background-color: black;
    }
</style>

<body>

    <nav class="navbar header">
        <div class="container-fluid">
            <div class="navbar-header ">
                <?php echo $title; ?>

                <button type="button" class="btn btn-warning"><a href="<?php echo site_url('news/create'); ?>">create
                        News</a></button>
            </div>

    </nav>


</body>

</html>
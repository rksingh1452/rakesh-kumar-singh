<?php

class Form extends CI_Controller
{

    public function index()
    {
        $this->load->helper(array('form', 'url'));

        $this->load->library('form_validation');

        $this->form_validation->set_rules('username', 'Username', 'callback_username_check');

        $this->form_validation->set_rules(
            'password',
            'Password',
            'trim|required|min_length[8]',
            //custom error message
            array('required' => 'Please fill the {field}')
        );

        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required|matches[password]');

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

        //Setting Error Messages :-
        $this->form_validation->set_message('required', 'fill {field}');

        $this->form_validation->set_message('min_length', '{field} should have at least {param} characters.');

        //changing error delimeter globally
        // $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        //setting rules using an array
        // $config = array(
        //     array(
        //         'field' => 'username',
        //         'label' => 'Username',
        //         'rules' => 'required|min_length[5]|max_length[12]|is_unique[users.username]'
        //     ),
        //     array(
        //         'field' => 'password',
        //         'label' => 'Password',
        //         'rules' => 'required',
        //         'errors' => array(
        //             'required' => 'You must provide a %s.',
        //         ),
        //     ),
        //     array(
        //         'field' => 'passconf',
        //         'label' => 'Password Confirmation',
        //         'rules' => 'required|matches[password]'
        //     ),
        //     array(
        //         'field' => 'email',
        //         'label' => 'Email',
        //         'rules' => 'required|valid_email|is_unique[users.email]'
        //     )
        // );
        //repopulate for with previous data
        set_value('field name');

        //used to set validation using array
        // $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('myform');
        } else {
            $this->load->view('formsuccess');
        }

    }


    public function username_check($str)
    {
        if ($str == 'test') {
            $this->form_validation->set_message('username_check', 'The {field} field can not be the word "test"');
            return FALSE;
        } else {
            return TRUE;
        }
    }


}
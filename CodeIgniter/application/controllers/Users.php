<?php
class Users extends MY_Controller
// MY_controller coz we extender CI_Controller property in My_Controller in application/core/My_Controller.php
{
        public function user()
        {
                //$this->load->helper('url');

                //loading library
                $this->load->library('test');
                $this->test->dbdetails(); //dbdetails is the function in libraries/test.php


                //dynamically pass data into library using 2nd parameter
                // $this->load->library('test');
                // $this->test->some_method();
                // $para = array('type' => 'car', 'color' => 'red');
                // $this->load->library('test', $para);

                //extended libraries
                //$this->load->library('email');//data is auto load
                $this->email->show();

                //$this->load->model('usermodel');//data is autoload 
                $data['users'] = $this->usermodel->getuserdata();

                //core class
                $this->test(); //test function in Mycontroller.php
                //load is not required here coz CI provide load here 

                // custom helper
                $this->load->helper('xyz');
                test();
                //extended helper
                $this->load->helper("array");
                show(); //it is function in My_array_helper.php

                //benchmark class (library)
                $this->benchmark->mark('code_start');

                $this->load->view('users/userslist', $data);

                $this->benchmark->mark('code_end');
                echo $this->benchmark->elapsed_time('code_start', 'code_end');

                echo $this->benchmark->memory_usage('code_start', 'code_end');

                // calender class
                // $this->load->library('calendar');
                // $data = array(
                //     3 => 'http://localhost/codeIgniter/index.php/welcome/about',
                //     7 => 'http://example.com/news/article/2006/06/07/',
                //     13 => 'http://example.com/news/article/2006/06/13/',
                //     26 => 'http://example.com/news/article/2006/06/26/',
                // );

                // echo $this->calendar->generate(2006, 6, $data, );

                // $prefs = array(
                //     'show_next_prev' => TRUE,
                //     'next_prev_url' => 'http://example.com/index.php/calendar/show/',
                //     'start_day' => 'saturday',
                //     'month_type' => 'long',
                //     'day_type' => 'short'
                // );

                // $this->load->library('calendar', $prefs);

                // echo $this->calendar->generate($this->uri->segment(3), $this->uri->segment(4));

                //creating table preference

                //         $prefs['template'] = '

                //         {table_open}<table border="0" cellpadding="0" cellspacing="0">{/table_open}

                //         {heading_row_start}<tr>{/heading_row_start}

                //         {heading_previous_cell}<th><a href="{previous_url}">&lt;&lt;</a></th>{/heading_previous_cell}
//         {heading_title_cell}<th colspan="{colspan}">{heading}</th>{/heading_title_cell}
//         {heading_next_cell}<th><a href="{next_url}">&gt;&gt;</a></th>{/heading_next_cell}

                //         {heading_row_end}</tr>{/heading_row_end}

                //         {week_row_start}<tr>{/week_row_start}
//         {week_day_cell}<td>{week_day}</td>{/week_day_cell}
//         {week_row_end}</tr>{/week_row_end}

                //         {cal_row_start}<tr>{/cal_row_start}
//         {cal_cell_start}<td>{/cal_cell_start}
//         {cal_cell_start_today}<td>{/cal_cell_start_today}
//         {cal_cell_start_other}<td class="other-month">{/cal_cell_start_other}

                //         {cal_cell_content}<a href="{content}">{day}</a>{/cal_cell_content}
//         {cal_cell_content_today}<div class="highlight"><a href="{content}">{day}</a></div>{/cal_cell_content_today}

                //         {cal_cell_no_content}{day}{/cal_cell_no_content}
//         {cal_cell_no_content_today}<div class="highlight">{day}</div>{/cal_cell_no_content_today}

                //         {cal_cell_blank}&nbsp;{/cal_cell_blank}

                //         {cal_cell_other}{day}{/cal_cel_other}

                //         {cal_cell_end}</td>{/cal_cell_end}
//         {cal_cell_end_today}</td>{/cal_cell_end_today}
//         {cal_cell_end_other}</td>{/cal_cell_end_other}
//         {cal_row_end}</tr>{/cal_row_end}

                //         {table_close}</table>{/table_close}
// ';

                //         $this->load->library('calendar', $prefs);

                //         echo $this->calendar->generate();

                //config file:-
                $this->config->load('create_config');

                echo $this->config->item('language');

                $server = $this->config->item('server');
                echo $server['host'];


                // //emial class:-
                // $this->load->library('email');

                // $this->email->from('rksingh1452@gmail.com', 'Rakesh Singh');
                // $this->email->to('rakeshcool118@gmail.com');
                // $this->email->subject('Email Test');
                // $this->email->message('Testing the email class.');

                // $this->email->send();

                // $config['protocol'] = 'sendmail';
                // $config['mailpath'] = '/usr/sbin/sendmail';
                // $config['charset'] = 'iso-8859-1';
                // $config['wordwrap'] = TRUE;

                // $this->email->initialize($config);

                echo '<br>';
                //pagination :-
                $this->load->library('pagination');

                $config['base_url'] = 'http://localhost/codeIgniter/index.php/users/user';
                $config['total_rows'] = 200;
                $config['per_page'] = 20;
                // $config['use_page_numbers'] = TRUE;
                // $config['page_query_string'] = TRUE;
                // $config['query_string_segment'] = 'your_string';

                $config['full_tag_open'] = '<p>';

                $config['full_tag_close'] = '</p>';

                // Customizing the First Link
                $config['first_link'] = 'First';
                $config['first_tag_open'] = '<div>';
                $config['first_tag_close'] = '</div>';

                $config['first_url'] = 'http://localhost/codeIgniter/index.php/users/user';

                // Customizing the Last Link
                $config['last_link'] = 'Last';
                // Customizing the “Next” Link
                $config['next_link'] = '&gt;';
                // Customizing the “Current Page” Link
                $config['cur_tag_open'] = '<b>';
                $config['cur_tag_close'] = '</b>';


                $this->pagination->initialize($config);
                echo $this->pagination->create_links();


                // session class:-
                $this->load->library('session');
                $this->session;
                //Retriving sessions data:-
                echo $this->session->userdata('item');
                $name = $this->session->userdata('');
                echo $name;


                //adding a session data
                $newdata = array(
                        'username' => 'johndoe',
                        'email' => 'johndoe@some-site.com',
                        'logged_in' => TRUE
                );

                $this->session->set_userdata($newdata);
                // add userdata one value at a time
                $this->session->set_userdata('Rakesh', 45);
                $this->session->set_userdata('Ramesh', 35);
                $this->session->set_userdata('Bikash', 20);
                $this->session->set_userdata('Anchal', 55);
                // verify that a session value exists
                // echo isset($_SESSION['Rakesh']);
                // or
                // echo $this->session->has_userdata('Bikash');
                // Removing Session Data
                unset($_SESSION['Rakesh']); //or
                $this->session->unset_userdata('Rakesh');

                //destroying a session
                // $this->session->sess_destroy();

                // Accessing session meta data
                // echo session_id() . '<br>';

                // echo $_SERVER['REMOTE_ADDR'] . '<br>';

                // echo $this->input->user_agent();

                //database driver
                $config['sess_driver'] = 'database';
                $config['sess_save_path'] = 'ci_sessions';



        }

}
?>
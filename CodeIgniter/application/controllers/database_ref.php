<?php
class Database_ref extends CI_Controller
{
    public function data_ref()
    {
        $this->load->database();

        //Standard Query With Multiple Results (Object Version)
        $query = $this->db->query('SELECT ID,FirstName,AccountNo from userdetails');

        foreach ($query->result() as $row) {
            echo $row->ID;
            echo $row->FirstName;
            echo $row->AccountNo;
        }
        echo 'Total Result: ' . $query->num_rows();
        echo '<br>';

        //Standard Query With Multiple Results (Array Version)
        foreach ($query->result_array() as $row) {
            echo $row['ID'];
            echo $row['FirstName'];
            echo $row['AccountNo'];
        }

        // //standard Insert
        // $sql = "INSERT INTO userdetails (ID, FirstName) VALUES (" . $this->db->escape(4) . ", " . $this->db->escape('Vikram') . ")";
        // $this->db->query($sql);
        // echo $this->db->affected_rows();

        //Query Builder Query:- gives you a simplified means of retrieving data
        echo "<br>";
        $query = $this->db->get('userdetails');
        foreach ($query->result() as $row) {
            echo $row->FirstName;
        }

        echo '<br>';

        //connecting to multiple database
        $DB1 = $this->load->database('group1', TRUE);
        $query = $DB1->query('SELECT cID,cName,cEmail from customers'); //query

        foreach ($query->result() as $row) {
            echo $row->cID; //generating query result using objects
            echo $row->cName;
            echo $row->cEmail . "<br>";
        }

        //returns last query that was run
        $str = $this->db->last_query(); //SELECT * FROM `userdetails`
        echo "$str";
        echo '<br>';
        //determine the number of rows in a particular table
        echo $this->db->count_all('userdetails'); //4
        echo "<br>";
        echo $this->db->platform(); //mysqli
        echo "<br>";
        echo $this->db->version(); //5.5.5-10.1.36-MariaDB

        //process of writing database inserts
        echo $this->db->count_all('ci_sessions'); //0
        echo '<br>';
        //query builder class:-

        $query = $this->db->get('userdetails'); //Selecting Data

        foreach ($query->result() as $row) {
            echo $row->FirstName;
        }

        $sql = $this->db->get_compiled_select('userdetails');
        echo $sql; //SELECT * FROM `userdetails`

        //SELECT MAX(field)
        $this->db->select_max('ID');
        $query = $this->db->get('userdetails');
        foreach ($query->result() as $row) {
            echo $row->ID; //3
        }
        //$this->db->select_min()
        $this->db->select_min('ID');
        $query = $this->db->get('userdetails');
        foreach ($query->result() as $row) {
            echo $row->ID; //1
        }
        //looking for specific data
        $array = array('ID' => 1, 'FirstName' => 'Rakesh', 'AccountNo' => 890675);

        // //Inserting Data
        // $data = array(
        //     'ID' => 5,
        //     'FirstName' => 'Avkit',
        //     'AccountNo' => 345112
        // );

        // $this->db->insert('userdetails', $data);
        $this->db->order_by('ID', 'ASC');

        //updating data
        // $data = array(
        //     'ID' => 6,
        //     'FirstName' => 'Moosam',
        //     'AccountNo' => 610656
        // );
        // $this->db->where('id', NULL);
        // $this->db->replace('userdetails', $data);

        //Deleting Data:-
        // $this->db->delete('userdetails', array('id' => NULL));

        //Transactions
        // $this->db->trans_start();
        // $this->db->query('DELETE FROM userdetails WHERE ID=6');

        // $this->db->query('INSERT INTO userdetails
        // VALUES (6, "Kartik", 098324)');

        // $this->db->query('UPDATE userdetails
        // SET FirstName = "Aman"
        // WHERE ID = 5');

        // $this->db->trans_complete();

        //Running Transactions Manually
        // $this->db->trans_begin();
        // $this->db->query('DELETE FROM userdetails WHERE ID=6');

        // $this->db->query('INSERT INTO userdetails
        // VALUES (6, "Kartik", 098324)');

        // $this->db->query('UPDATE userdetails
        // SET FirstName = "Aman"
        // WHERE ID = 5');

        // if ($this->db->trans_status() === FALSE) {
        //     $this->db->trans_rollback();
        // } else {
        //     $this->db->trans_commit();
        // }

        // Download Helper:-
        // $this->load->helper('download');
        // $data = 'Here is some text!';
        // $name = 'mytext.txt';
        // force_download($name, $data);

        echo "<br>";
        // Email Helper
        // $this->load->helper('email');
        // if (valid_email('rksingh1452@gmail.com')) {
        //     echo 'email is valid';
        // } else {
        //     echo 'email is not valid';
        // }
        // mail('rakeshcool118@gmail.com', "testing mail", "email helper");

        //url helper
        $this->load->helper('url'); //loading
        echo site_url('users/user'); //http://[::1]/codeIgniter/index.php/users/user
        echo "<br>";
        //segment pass through an array
        $segments = array('users', 'user');
        echo site_url($segments);
        echo "<br>";
        echo base_url(); //http://[::1]/codeIgniter/
        echo "<br>";
        echo site_url('users/user'); //http://[::1]/codeIgniter/index.php/users/user
        echo "<br>";
        echo current_url(); //url of current viewing page
        echo "<br>";
        echo index_page();
        echo "<br>";
        //anchor($uri = '', $title = '', $attributes = '')
        echo anchor('users/user', 'Go to user page', 'title="News title"');
        echo "<br>";
        echo anchor('users/user', 'Click here');
        echo "<br>";
        //anchor_popup($uri = '', $title = '', $attributes = FALSE) 
        $atts = array(
            'width' => 15000,
            'height' => 1000,
            'scrollbars' => 'yes',
            'status' => 'yes',
            'resizable' => 'yes',
            'screenx' => 0,
            'screeny' => 0,
            'window_name' => '_blank'
        );

        echo anchor_popup('users/user', 'Click Me!', $atts);
        echo "<br>";
        //mailto($email, $title = '', $attributes = '')
        echo mailto('rakeshcool118@gmail.com', 'Click Here to Contact Me');
        echo "<br>";
        // set attributes using the third parameter:
        $attributes = array('title' => 'Mail me');
        echo mailto('me@my-site.com', 'Contact Me', $attributes);
        echo "<br>";
        // url_title($str, $separator = '-', $lowercase = FALSE)
        $title = "What's wrong with CSS?";
        echo $url_title = url_title($title);
        $title = "What's wrong with CSS?";
        echo "<br>";
        echo $url_title = url_title($title, 'underscore', TRUE);
        echo "<br>";
        // prep_url($str = '')
        echo $url = prep_url('example.com');

        // if ($logged_in == FALSE) {
        //     redirect('/form/');
        // }

        // // with 301 redirect
        // redirect('/users/user/', 'location', 301);
    }
}
?>
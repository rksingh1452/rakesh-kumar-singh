<!DOCTYPE html>
<html lang="en">

<head>
    <title>Todo-List</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style>
        #delbutton{
        display: inline-block;
        margin-left:50px;
        }
        .lines{
            font-size:22px;
        }
        .tableFont{
            font-weight: bold;
            font: size 33px;
        }
    </style>
</head>

<body>
    <div class="container">
        <h2>List of task :-</h2>

        <!-- update flash message -->
        <?php if($update = $this->session->flashdata('update')): ?>        
        <div class="row">
        <div class="col-lg-6">
        <div class=" alert alert-success ">
            <?php echo $update;?>
        </div>
        </div>  
    </div>         
    <?php endif; ?> 

    <!-- add flash message -->
    <?php if($add = $this->session->flashdata('add')): ?>        
        <div class="row">
        <div class="col-lg-6">
        <div class=" alert alert-success">
            <?php echo $add;?>
        </div>
        </div>  
    </div>         
    <?php endif; ?> 

    <!-- delete flash message -->
    <?php if($delete = $this->session->flashdata('delete')): ?>        
        <div class="row">
        <div class="col-lg-6">
        <div class=" alert alert-danger">
            <?php echo $delete;?>
        </div>
        </div>  
    </div>         
    <?php endif; ?>         


        <table class="table table-condensed">
            <thead  class="tableFont">
                <td>Sl No.</td>
                <td>Task</td>
                <td>Delete</td>
                <td>Update</td>
            </thead>

            <?php
            $i=1;
            foreach ($items as $item): 
                {
                    echo "<tbody>";
                    echo "<td>" .$i."</td>";
                    echo "<td>".$item->Task."</td>";
                    // delete :-
                    echo "<td>".
                    form_open('Todo/deleteTask'),
                    form_hidden('id',$item->id),
                    form_submit(['name'=>'submit', 'value'=>'Delete', 'class'=>'btn btn-sm btn-danger','onclick'=>"return myFunction()"]),
                    form_close().           
                   "</td>";
                    // update
                //    echo "<td class='btn btn-info'> <a href='editdata?id=".$item->id."'>Update</a> </td>";

                     echo "<td>".
                     form_open('update'),    
                     form_hidden('id',$item->id),
                     form_submit(['name'=>'submit', 'value'=>'Update', 'class'=>'btn btn-sm btn-info']),
                     form_close().           
                    "</td>";

                    echo "</tbody>";
                    $i++;
                }
            endforeach;
            ?>
        </table>
        <button type="button" class="btn btn-success btn-success"><a
                href="<?php echo site_url('add_task'); ?>">Add Task</a></button>
                <!--echo site_url('Todo/formcreate'); check route-->

    <script>
    function myFunction() {
    if(confirm("Data will be deleted permanantly!") == true)
        {
         return true;
        }
        else{
            return false;
        }
        }
     </script>




        <!--items is key define in controller -->
        
        <ol>
            <tr>
             <?php /* foreach ($items as $item): ?>  
                <li class='lines'>
                  <td><?php echo $item->Task; ?></td>  
                       
                    <!-- delete database -->
                    <td>
                   <span id='delbutton';>
                  <?php  echo form_open('Todo/deleteTask/'),
                     form_hidden('id',$item->id),
                    form_submit(['name'=>'submit', 'value'=>'Delete', 'class'=>'btn btn-sm btn-danger']),
                    form_close();
                 ?>  
                 </td> 
                 </span> 
                 </li>  
                 </tr>
                  
               
            <?php endforeach; ?>
        </ol>
               
        <button type="button" class="btn btn-success btn-success"><a
                href="<?php echo site_url('Todo/formcreate'); ?>">Add Task</a></button> 
                */  
                ?>
    </div>

</body>

</html>
<html>

<head>
    <title>Edit Your Task</title>
</head>
<style>
    .error{
        color:red;
    }
</style>
<body>

    <h3>Edit Task</h3>
    
    <?php echo form_open('Todo/update_data'); ?>
    <?php echo form_hidden(['id'=>$list->id]);?>
    <label for="task">Task</label>
    <?php
    $data = array(        
        'value' => $list->Task,
        'name' => 'task'        
        );

    echo form_input($data);
     echo validation_errors('<span class="error">', '</span>'); 
    ?>
    
    <br><br>

    <input type="submit" name="submit" value="update"  />

    </form>              

</body>

</html>
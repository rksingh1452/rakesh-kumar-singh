<?php
class Todo_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        
    }

    public function get_task()
    {       
        $this->db->select('Task, id');
        $query=$this->db->get('todolist');
        return $query->result();  
    }

    public function set_task()
    {        
        $data = array(
            'Task' => $this->input->post('task')
        );
        return $this->db->insert('todolist', $data);
    }

    public function delete_task($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('todolist'); 
        $this->db->affected_rows();       
    }
//update task :-

public function find_task($id)
{
    $this->db->select(['Task','id']);
    $this->db->where('id',$id);
    $query=$this->db->get('todolist');
    return $query->row();
    
}
    public function update_task($id)
    {
        $data = array(
            'Task' => $this->input->post('task')
        );
        // $this->db->where('id',$id);
        return $this->db->update('todolist', $data, array('id' => $id));
    
       
        // return $this->db->update('todolist',$data);
    }



}

?>
<?php
class Todo extends CI_Controller
{
    public function __construct()
    {
                parent::__construct();
        $this->load->model('Todo_model');
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('session');

    }
    public function index()
    {
        $this->load->view("template/header");
        

        $data['items'] = $this->Todo_model->get_task();

        $this->load->view('List', $data);
        $this->load->view("template/footer");
    }

    public function view()
    {
        $this->load->view("template/header");
      

        $data['items'] = $this->Todo_model->get_task();
        $this->load->view('List', $data);

        $this->load->view("template/footer");
    }

    public function formcreate()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('task', 'Task', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('createform');

        } else {
            $this->Todo_model->set_task();
            $this->session->set_flashdata('add','Your task is Added successfully');
            redirect('todo-list');

            // $this->load->view("template/header");
            
            // $data['items'] = $this->Todo_model->get_task();
            // $this->load->view('List', $data);

            // $this->load->view('success');
            // $this->load->view("template/footer");
        }
    }

    public function deleteTask()
        {        
        //$id = $this->uri->segment('3')
       $id=$this->input->post('id');           
       $del=$this->Todo_model->delete_task($id);  
        
       if($del==false){
        $this->session->set_flashdata('delete','Your task is delete successfully');
       redirect('todo-list');   
       
       }    
        
    }

    //update task :-
    public function editdata()
    {
        $id=$this->input->post('id');       

        $rt=$this->Todo_model->find_task($id);
        // print_r($rt);                              
        // $this->load->view('update_form',['todolist'=>$rt]);
        

        $this->load->library('form_validation');

        $this->form_validation->set_rules('task', 'Task', 'required',
        array('required' => 'You must provide a %s.')
        );

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('update_form',['list'=>$rt]);

        } else {
           echo "error";
           
        }        
    }

    public function update_data()
    {
        $id=$this->input->post('id'); 
        // $task=$this->input->post('task');
        // print_r($id);
        // print_r($this->input->post());
        if($this->Todo_model->update_task($id))
        {
            $this->session->set_flashdata('update','Your task is updated successfully');
              
        }
        // else
        // {

        // }
        // echo "successfully updated";
        redirect('todo-list');
    }
  



}

?>
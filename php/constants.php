<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Constants</title>
</head>

<body>
    <h3>PHP Constants:-</h3>
    <p>Constants are like variables except that once they are defined they cannot be changed or undefined.</p>

    <h3>Create a PHP Constant:-</h3>
    <p>To create a constant, use the define() function.</p>
    <!-- syntax:-
        define(name, value, case-insensitive) 
        parameters:-
            name: Specifies the name of the constant
            value: Specifies the value of the constant
            case-insensitive: Specifies whether the constant name should be case-insensitive. Default is false-->

    <?php
    //1. Create a PHP Constant:-
    //i. case-sensative:-
    define("greetings", "Welcome to dhanbad!");
    echo greetings; //Welcome to dhanbad!
    echo "<br>";

    //ii. case insensative:-
    define("Rakesh", "welcome home!", true);
    echo rakesh; //welcome home!
    echo "<br>";

    //2. PHP Constant Arrays :-
    define("cars", [
        'tata-safari',
        'xuv-700',
        'thar'
    ]);
    echo cars[0]; // tata-safari
    echo "<br>";

    //3. Constants are Global:-Constants are automatically global and can be used across the entire script.
    define("fruits", "fruits are good for health");
    function rakesh()
    {
        echo fruits;
    }
    rakesh(); //fruits are good for health
    
    ?>
</body>

</html>
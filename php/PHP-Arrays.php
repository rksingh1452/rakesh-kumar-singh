<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Arrays</title>
</head>

<body>
    <h3>PHP Arrays :-</h3>
    <p>An array stores multiple values in one single variable:</p>
    <?php
    $cars = array("volvo", "tata", "mahindra");
    echo "I like " . $cars[0] . " ," . $cars[1] . " ," . $cars[2] . " .";
    echo "<br>";

    //The count() function :- used to return the length (the number of elements) of an array:
    echo count($cars);
    echo "<br>"; // 3
    ?>

    <h3>Three types of arrays:-</h3>
    <h3>1. Indexed arrays:-</h3>
    <?php
    $cars = array("volvo", "tata", "mahindra");
    echo "I like " . $cars[0] . " ," . $cars[1] . " ," . $cars[2] . " .";
    echo "<br>";

    //Loop Through an Indexed Array:-
    $fruits = array("apple", "mango", "banana", "mango");
    $arrlen = count($fruits);
    for ($x = 0; $x < $arrlen; $x++) {
        echo $fruits[$x];
        echo "<br>";
    }
    ?>
    <h3>2. Associative arrays:-</h3>
    <p>Associative arrays are arrays that use named keys that you assign to them</p>

    <!-- two ways to create associative arrays;-
        $age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");
        or
        $age['Peter'] = "35";
        $age['Ben'] = "37";
        $age['Joe'] = "43";-->
    <?php
    //The named keys can then be used in a script:-
    $age = array("peter" => "35", "Ben" => "37", "Joe" => "43");
    echo "Peter is " . $age['peter'] . " years old.";
    echo "<br>";

    //Loop Through an Associative Array
    foreach ($age as $x => $x_value) {
        echo "key= " . $x . ", value = " . $x_value;
        echo "<br>";
    }
    ?>
    <h3>3. Multidimensional arrays</h3>
    <p>A two-dimensional array is an array of arrays (a three-dimensional array is an array of arrays of arrays).</p>
    <?php
    $cars = array(
        array("Volvo", 22, 18),
        array("BMW", 15, 13),
        array("Saab", 5, 2),
        array("Land Raver", 17, 15)
    );
    echo $cars[0][0] . " : In stock : " . $cars[0][1] . " ,Sold : " . $cars[0][2] . ".<br>";
    echo $cars[1][0] . " : In stock : " . $cars[1][1] . " ,Sold : " . $cars[1][2] . ".<br>";
    echo $cars[2][0] . " : In stock : " . $cars[2][1] . " ,Sold : " . $cars[2][2] . ".<br>";
    echo $cars[3][0] . " : In stock : " . $cars[3][1] . " ,Sold : " . $cars[3][2] . ".<br>";

    //for loop inside for loop
    echo "<h3>We can also put a for loop inside another for loop to get the elements of the cars array (we still have to point to the two indices):</h3>";
    for ($row = 0; $row < 4; $row++) {
        echo "<b>Row number: $row</b> <br>";

        echo "<ul>";
        for ($col = 0; $col < 3; $col++) {
            echo "<li>" . $cars[$row][$col] . "</li>";
        }
        echo "</ul>";
    }

    ?>
</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Data Types</title>
</head>

<body>
    <?php
    // 1. PHP String :-A string is a sequence of characters, like "Hello world!".
    $a = "hello world!";
    $b = 'hello World ';
    echo $a;
    echo "<br>";
    echo $b;
    echo "<br>";

    //2. PHP Integer :-An integer data type is a non-decimal number between -2,147,483,648 and 2,147,483,647.
    $x = 5985;
    var_dump($x); //var_dump return datatype and value. op- int(5985)
    echo "<br>";


    // 3. PHP Float:- A float (floating point number) is a number with a decimal point or a number in exponential form.
    $x1 = 10.656;
    var_dump($x1);
    echo "<br>";

    // 3. PHP Boolean :-A Boolean represents two possible states: TRUE or FALSE.
    $x3 = true;
    $x4 = false;
    var_dump($x3);
    echo "<br>";
    var_dump($x4);
    echo "<br>";

    // 4. PHP Array :-An array stores multiple values in one single variable.
    $cars = array("volvo", "BMW", "tata");
    var_dump($cars);
    echo "<br>";

    // 5. PHP NULL Value :-Null is a special data type which can have only one value: NULL.
    $y = "hello world";
    $y = null;
    var_dump($y);
    echo "<br>";

    // 6. PHP Object :-Classes and objects are the two main aspects of object-oriented programming.
    
    //A class is a template for objects, and an object is an instance of a class.
    class Car
    {
        public $color;
        public $model;
        public function __construct($color, $model)
        {
            $this->color = $color;
            $this->model = $model;
        }
        public function message()
        {
            return "my car is a " . $this->color . " " . $this->model . "!";
        }
    }
    $mycar = new Car("black", "volvo");
    echo $mycar->message();
    echo "<br>";
    $mycar = new Car("red", "toyota");
    echo $mycar->message();




    ?>
</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Forms - Validate E-mail and URL</title>
</head>

<body>
    <h3>PHP Forms - Validate E-mail and URL :-</h3>
    <p>This chapter shows how to validate names, e-mails, and URLs.</p>

    <h3>1. PHP-Validate Name :-</h3>
    <p>The code below shows a simple way to check if the name field only contains letters, dashes, apostrophes and
        whitespaces. If the value of the name field is not valid, then store an error message:</p>

    $name = test_input($_POST["name"]);
    if (!preg_match("/^[a-zA-Z-' ]*$/",$name)) {
    $nameErr = "Only letters and white space allowed";
    }

    <h3>2. PHP - Validate E-mail :-</h3>
    <p>The easiest and safest way to check whether an email address is well-formed is to use PHP's filter_var()
        function.</p>

    $email = test_input($_POST["email"]);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $emailErr = "Invalid email format";
    }

    <h3>3. PHP - Validate URL :-</h3>
    <p>The code below shows a way to check if a URL address syntax is valid (this regular expression also allows dashes
        in the URL). If the URL address syntax is not valid, then store an error message:</p>

    $website = test_input($_POST["website"]);
    if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$website)) {
    $websiteErr = "Invalid URL";
    }
</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Form Handling</title>
</head>

<body>
    <h2>PHP Form Handling :-</h2>
    <p>The PHP superglobals $_GET and $_POST are used to collect form-data.</p>

    <h3>PHP - A Simple HTML Form $_POST method:-</h3>
    <form action="welcome.php" method="post">
        <label for="name">Name: </label>
        <input type="text" name="name" id="name"><br>

        <label for="email">Email: </label>
        <input type="text" name="email" id="email"><br>
        <input type="Submit">
    </form>

    <h3>GET method:-</h3>
    <form action="welcome.php" method="get">
        <label for="name">Name: </label>
        <input type="text" name="name" id="name"><br>

        <label for="email">Email: </label>
        <input type="text" name="email" id="email"><br>
        <input type="Submit">
    </form>

</body>

</html>
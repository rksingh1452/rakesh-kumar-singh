<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Strings</title>
</head>

<body>
    <?php
    //1. strlen() - Return the Length of a String
    echo strlen("hello world!"); // op- 12
    echo "<br>";

    //2. str_word_count() - Count Words in a String
    echo str_word_count("Hello world!");
    echo "<br>";

    // 3. strrev() - Reverse a String
    echo strrev("hello world!"); // op- !dlrow olleh
    echo "<br>";

    //4. strpos() - Search For a Text Within a String.
    // If a match is found, the function returns the character position of the first match. If no match is found, it will return FALSE.
    //The first character position in a string is 0 (not 1).
    
    echo strpos("hello world", "world"); // op - 6
    echo "<br>";

    //5. str_replace() - Replace Text Within a String
    echo str_replace("world", "dolly", "hello world!"); //op-hello dolly!
    echo "<br>";





    ?>

</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP if...else...elseif and switch Statement</title>
</head>

<body>
    <h3>PHP if...else...elseif Statements :-</h3>
    <p>1. PHP - The if Statement</p>
    <?php
    $t = date("H");
    echo $t;
    echo "<br>";
    if ($t < "20") {
        echo "Have a good day";
    }

    ?>
    <p>2. PHP - The if...else Statement</p>
    <?php
    $t = date("H");

    if ($t < "20") {
        echo "Have a good day!";
    } else {
        echo "Have a good night!";
    }
    ?>
    <p>3. PHP - The if...elseif...else Statement</p>
    <?php
    $t = date("H");
    echo "<p>The hour (of the server) is " . $t;
    echo ", and will give the following message:</p>";

    if ($t < "10") {
        echo "Have a good morning!";
    } elseif ($t < "20") {
        echo "Have a good day!";
    } else {
        echo "Have a good night!";
    }
    ?>

    <h3>The PHP switch Statement :-</h3>
    <?php
    $x = rand(10, 50);
    echo $x;
    echo "<br>";
    switch ($x) {
        case ($x < 20):
            echo "Your number is between 10 to 19!";
            break;
        case ($x >= 20 and $x < 30):
            echo "Your  number is between 20 to 29";
            break;
        case ($x >= 30 and $x < 40):
            echo "Your number is between 30 to 39!";
            break;
        default:
            echo "Your number is between 40 to 50!!";
    }

    ?>
</body>

</html>
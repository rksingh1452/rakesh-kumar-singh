<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>syntax, comments, variables, echo/print</title>
</head>

<body>
    <!-- syntax:--->
    <?php
    echo "hello World <br>";
    echo "hello world <br>"; //not case sensative
    

    //$color, $COLOR, and $coLOR are treated as three different variables because they are user defined variables and are case sensative :-
    $color = "red";
    echo "My car is " . $color . "<br>";
    // echo "My house is " . $COLOR . "<br>";
// echo "My boat is " . $coLOR . "<br>";
    
    //single line comments in php
#This is also single line comments in php
    
    /*this is
    multi line 
    comments in php */

    // # PHP Variables :- Variables are "containers" for storing information.
    
    // 1. Creating (Declaring PHP Variables) :-a variable starts with the $ sign, followed by the name of the variable.
    
    $text = "Hello World";
    $x = 5;
    $y = 10.5;

    //Output Variables :- PHP echo statement is often used to output data to the screen.
    echo "$text" + "<br>";
    echo $x;
    echo "<br>";
    echo $y . "<br>";

    echo " i love " . $text . "!";

    // PHP Variables Scope:- 
    //  three different variable scopes:-
    // 1. Local
    // 2. global
    // 3. static
    
    // 1. Global Scope :-A variable declared outside a function has a GLOBAL SCOPE and can only be accessed outside a function.
    
    $a = 10; // global scope
    
    function mytest()
    {
        // using x inside this function will generate an error
        // echo "<p> Variable a inside function is : $a </p>"
    }
    //mytext();
    
    echo "<p> variable a outside function is : $a </p>";

    //2. Local Scope :- A variable declared within a function has a LOCAL SCOPE and can only be accessed within that function.
    
    function mytest1()
    {
        $b = 5; // local scope
        echo "<p> Variable b inside function is : $b</p>";
    }
    mytest1();
    // using x outside the function will generate an error
    //echo " Variable b outside function is : $b"
    
    // php the global keyword :-The global keyword is used to access a global variable from within a function.
    $p = 5;
    $q = 10;
    function mytest2()
    {
        global $p, $q;
        $p = $p + $q;
    }
    mytest2();
    echo $p; // op - 15
    echo "<br>";

    //PHP also stores all global variables in an array called $GLOBALS[index]. The index holds the name of the variable. This array is also accessible from within functions and can be used to update global variables directly.
    $s = 20;
    $t = 30;
    function mytest3()
    {
        $GLOBALS['t'] = $GLOBALS['s'] + $GLOBALS['t'];
    }
    mytest3();
    echo $t;
    echo "<br>";

    // PHP The static Keyword:- 
    //Normally, when a function is completed/executed, all of its variables are deleted. However, sometimes we want a local variable NOT to be deleted. Use static keyword.
    function mytest4()
    {
        static $v = 0;
        echo $v;
        $v++;
    }
    mytest4();
    echo "<br>";
    mytest4();
    echo "<br>";
    mytest4();
    echo "<br>";

    // #PHP echo and print Statements:-
    
    // 1. echo has no return value while print has a return value of 1 so it can be used in expressions.
    
    // 2. echo can take multiple parameters (although such usage is rare) while print can take one argument. echo is marginally faster than print
    
    // The PHP echo Statement :-The echo statement can be used with or without parentheses: echo or echo().
    
    echo "<h2>PHP echo statement </h2>";
    echo "Hello World! <br>";
    echo "this ", "string ", "was ", "made ", "with multiple parameters. <br>";

    //Display Variables with echo :-
    $txt1 = "learn Php ";
    $txt2 = "w3schools";
    $x1 = 8;
    $y1 = 9;
    echo "<h2>" . $txt1 . "</h2>";
    echo "Study php at " . $txt2 . "<br>";
    echo $x1 + $y1;

    //The PHP print Statement :-The print statement can be used with or without parentheses: print or print().
    print "<h2>PHP is Fun!</h2>";
    print "Hello world!<br>";
    print "I'm about to learn PHP!";

    // Display Variables :-
    $txt11 = "Learn PHP";
    $txt22 = "W3Schools.com";
    $x2 = 5;
    $y2 = 4;
    print "<h2>" . $txt1 . "</h2>";
    print "Study PHP at " . $txt2 . "<br>";
    print $x + $y;




    ?>


</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Sorting Arrays</title>
</head>

<body>
    <h3>PHP - Sort Functions For Arrays :-</h3>

    <h3>1. sort() - sort arrays in ascending order</h3>
    <?php
    $cars = array("Volvo", "BMW", "Toyota");
    sort($cars);
    $len = count($cars);
    for ($x = 0; $x < $len; $x++) {
        echo $cars[$x];
        echo "<br>";
    }
    // sorting number array:-
    $numbers = array(4, 6, 2, 22, 11);
    sort($numbers);

    $arrlength = count($numbers);
    for ($x = 0; $x < $arrlength; $x++) {
        echo $numbers[$x];
        echo "<br>";
    }
    ?>

    <h3>2. rsort() - sort arrays in descending order</h3>
    <?php
    $cars = array("Volvo", "BMW", "Toyota");
    rsort($cars);
    $len = count($cars);
    for ($x = 0; $x < $len; $x++) {
        echo $cars[$x];
        echo "<br>";
    }
    // sorting number array:-
    $numbers = array(4, 6, 2, 22, 11);
    rsort($numbers);

    $arrlength = count($numbers);
    for ($x = 0; $x < $arrlength; $x++) {
        echo $numbers[$x];
        echo "<br>";
    }
    ?>

    <h3>3. asort() - sort associative arrays in ascending order, according to the value</h3>
    <?php
    $age = array("Praksh" => "54", "Bharat" => "45", "Jagan" => "55");
    asort($age);
    foreach ($age as $x => $x_value) {
        echo "Key=" . $x . ", value= " . $x_value;
        echo "<br>";
    }
    ?>

    <h3>4. ksort() - sort associative arrays in ascending order, according to the key</h3>
    <?php
    $age = array("Praksh" => "54", "Bharat" => "45", "Jagan" => "55");
    ksort($age);
    foreach ($age as $x => $x_value) {
        echo "Key=" . $x . ", value= " . $x_value;
        echo "<br>";
    }
    ?>
    <h3>5. arsort() - sort associative arrays in descending order, according to the value</h3>
    <?php
    $age = array("Praksh" => "54", "Bharat" => "45", "Jagan" => "55");
    arsort($age);
    foreach ($age as $x => $x_value) {
        echo "Key=" . $x . ", value= " . $x_value;
        echo "<br>";
    }
    ?>
    <h3>6. krsort() - sort associative arrays in descending order, according to the key</h3>
    <?php
    $age = array("Praksh" => "54", "Bharat" => "45", "Jagan" => "55");
    krsort($age);
    foreach ($age as $x => $x_value) {
        echo "Key=" . $x . ", value= " . $x_value;
        echo "<br>";
    }
    ?>

</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Loops</title>
</head>

<body>
    <h3>1. PHP while loop :-</h3>
    <?php
    $x = 1;
    while ($x <= 5) {
        echo "The number is: $x <br>";
        $x++;
    }

    echo "This example counts to 100 by tens:- <br>";
    $y = 0;
    while ($y <= 100) {
        echo "The number is: $y <br>";
        $y += 10;
    }
    ?>
    <h3>1. PHP Do while loop :- first execute the code then check the condition</h3>
    <?php
    $x = 1;
    do {
        echo "The number is : $x <br>";
        $x++;
    } while ($x <= 5);

    echo "Run the loop 1st time while condition is false:- <br>";
    $x = 6;
    do {
        echo "The number is : $x <br>";
        $x++;
    } while ($x <= 5);
    ?>
    <h3>1. PHP For loop :-</h3>
    <?php
    for ($x = 0; $x <= 10; $x++) {
        echo "the number is : $x <br>";
    }

    echo "This example counts to 100 by tens:- <br>";
    for ($x = 0; $x <= 100; $x += 10) {
        echo "the number is : $x <br>";
    }

    ?>
    <h3>1. PHP Foreach loop :-</h3>
    <p>The foreach loop works only on arrays, and is used to loop through each key/value pair in an array.</p>
    <!-- syntax:-
            foreach ($array as $value) {
                code to be executed;
            } -->
    <?php
    $colors = array("red", "green", "blue", "yellow");
    foreach ($colors as $value) {
        echo "$value <br>";
    }

    echo "The following example will output both the keys and the values of the given array :- <br>";

    $age = array("peter" => "35", "Rohan" => "22", "Ankit" => "28");
    foreach ($age as $x => $val) {
        echo "$x =$val <br>";
    }

    ?>
    <h3>1. PHP break/continue :-</h3>
    <?php
    //break :-
    for ($x = 0; $x < 10; $x++) {
        if ($x == 4) {
            break;
        }
        echo "The number is: $x <br>";
    }

    echo "The continue statement breaks one iteration (in the loop <br>";
    for ($x = 0; $x < 10; $x++) {
        if ($x == 4) {
            continue;
        }
        echo "The number is: $x <br>";
    }

    echo "Break and Continue in While Loop <br>";
    $x = 0;
    while ($x < 10) {
        if ($x == 4) {
            break;
        }
        echo "the number is : $x <br>";
        $x++;
    }
    echo "<br>";
    $y = 0;
    while ($y < 10) {
        if ($y == 4) {
            $y++;
            continue;
        }
        echo "the number is : $y <br>";
        $y++;
    }
    ?>
</body>

</html>
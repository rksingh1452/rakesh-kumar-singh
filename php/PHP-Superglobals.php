<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP-Superglobals</title>
</head>

<body>
    <h3>PHP Global Variables - Superglobals :-</h3>
    <p>Some predefined variables in PHP are "superglobals", which means that they are always accessible, regardless of
        scope - and you can access them from any function, class or file without having to do anything special.</p>
    <h3>1. $GLOBALS</h3>
    <p>i. $GLOBALS is a PHP super global variable which is used to access global variables from anywhere in the PHP
        script (also from within functions or methods).</p>
    <p>ii. PHP stores all global variables in an array called $GLOBALS[index]. The index holds the name of the variable.
    </p>
    <?php
    $x = 75;
    $y = 25;
    function addition()
    {
        $GLOBALS['z'] = $GLOBALS['x'] + $GLOBALS['y'];
    }
    addition();
    echo $z;
    ?>

    <h3>2. $_SERVER</h3>
    <p>$_SERVER is a PHP super global variable which holds information about headers, paths, and script locations.</p>
    <?php
    echo $_SERVER['PHP_SELF']; //op-/php/PHP-Superglobals.php;Returns the filename of the currently executing script
    echo "<br>";
    echo $_SERVER['SERVER_NAME']; //localhost
    echo "<br>";
    echo $_SERVER['HTTP_HOST']; // localhost
    echo "<br>";
    // echo $_SERVER['HTTP_REFERER'];
    echo "<br>";
    echo $_SERVER['HTTP_USER_AGENT'];
    echo "<br>";
    echo $_SERVER['SCRIPT_NAME']; ///php/PHP-Superglobals.php
    
    ?>

    <h3>3. REQUEST</h3>
    <p>used to collect data after submitting an HTML form.</p>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        Name: <input type="text" name="fname">
        <input type="submit">
    </form>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        //collect value of input feild
        $name = htmlspecialchars($_REQUEST['fname']);
        if (empty($name)) {
            echo "Name is empty";
        } else {
            echo $name;
        }
    }
    ?>

    <h3>4. POST</h3>
    <p>used to collect form data after submitting an HTML form with method="post". $_POST is also widely used to pass
        variables.</p>
    <?php

    ?>

    <h3>5. GET</h3>
    <p>used to collect form data after submitting an HTML form with method="get"</p>
    <a href="test_get.php?subject=PHP&web=W3schools.com">Test $GET</a>

</body>

</html>
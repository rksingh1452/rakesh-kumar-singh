<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Regular Expressions</title>
</head>

<body>
    <h3>PHP Regular Expressions</h3>
    <p>i. A regular expression is a sequence of characters that forms a search pattern. When you search for data in a
        text, you can use this search pattern to describe what you are searching for.</p>

    <p>ii. A regular expression can be a single character, or a more complicated pattern.</p>

    <p>iii. Regular expressions can be used to perform all types of text search and text replace operations.</p>

    <!-- SYNTAX :-
        $exp = "/w3schools/i"; 
    / is the delimiter, w3schools is the pattern that is being searched for, and i is a modifier that makes the search case-insensitive.
    when your pattern contains forward slashes it is convenient to choose other delimiters such as # or ~.-->
    <h3>1. preg_match():-Returns 1 if the pattern was found in the string and 0 if not</h3>
    <?php
    $str = "learn fullstack development from w3school";
    $pattern = "/w3school/i";
    echo preg_match($pattern, $str); //1
    ?>

    <h3>2. preg_match_all():-Returns the number of times the pattern was found in the string, which may also be 0</h3>
    <?php
    $str = "The rain in SPAIN falls mainly on the plains";
    $pattern = "/ain/i";
    echo preg_match_all($pattern, $str); //4
    ?>

    <h3>3. preg_replace():- The preg_replace() function will replace all of the matches of the pattern in a string with
        another string.</h3>
    <?php
    $str = "Visit Microsoft!";
    $pattern = "/microsoft/i";
    echo preg_replace($pattern, "w3school", $str); //Visit w3school!
    ?>

    <h3>4. Grouping :-</h3>
    <p>You can use parentheses ( ) to apply quantifiers to entire patterns. They also can be used to select parts of the
        pattern to be used as a match.</p>
    <?php
    $str = "Apples and bananas";
    $pattern = "/ba(na){2}/i";
    echo preg_match($pattern, $str); //1
    ?>
</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Math</title>
</head>

<body>
    <h3>PHP has a set of math functions that allows you to perform mathematical tasks on numbers.</h3>
    <?php
    //1. PHP pi() Function :-returns the value of PI:
    echo (pi()); //op-3.1415926535898
    echo "<br>";

    //2. PHP min() and max() Functions:-The min() and max() functions can be used to find the lowest or highest value in a list of arguments.
    echo (min(0, 150, 30, 20, -8, -200));
    echo "<br>";

    echo (max(0, 150, 30, 20, -8, -200)); // op- 150
    echo "<br>";

    //3. PHP abs() Function:-returns the absolute (positive) value of a number.
    echo (abs(-6.7)); //op-6.7
    echo "<br>";

    // 4. sqrt() function:- returns square root of a number.
    echo (sqrt(64)); //op- 8
    echo "<br>";

    // 5. round():- rounds a floating point numbers to its nearest integer.
    echo (round(0.60)); //op- 1
    echo "<br>";

    echo (round(0.49)); //op- 0
    echo "<br>";

    // 7. Random numbers:-generates a random number
    echo (rand());
    echo "<br>";

    //random integer between 10 to 100.
    echo (rand(10, 100));
    echo "<br>";
    ?>
</body>

</html>
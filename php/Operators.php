<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Operators</title>
</head>

<body>
    <h3>PHP Operators</h3>
    <p>i. Arithmetic operators</p>
    <?php
    $x = 10;
    $y = 8;
    //addition
    echo $x + $y; //18
    echo "<br>";

    //substraction
    echo $x - $y; //2
    echo "<br>";

    //multiplication
    echo $x * $y; //80
    echo "<br>";

    //division
    echo $x / $y; //1.25
    echo "<br>";

    //Modulus:- reminder of $x and $y
    echo $x % $y; //2
    echo "<br>";

    //exponential:-	Result of raising $x to the $y'th power
    echo $x ** $y; //100000000
    echo "<br>";
    ?>

    <p>ii. Assignment operators</p>
    <?php
    //1. x=y :-The left operand gets set to the value of the expression on the right
    $y = 10;
    $z = $y;
    echo $z; //10
    echo "<br>";

    // 2. x += y	(x = x + y)	Addition
    $x = 10;
    $y = 20;
    $x += $y;
    echo $x; // 30
    echo "<br>";

    //3. x -= y	(x = x - y)	Subtraction
    $x = 50;
    $x -= 30;
    echo $x; //20
    echo "<br>";

    //4. x *= y 	(x = x * y)	Multiplication
    $x = 10;
    $y = 20;
    $x *= $y;
    echo $x; // 200
    echo "<br>";

    //5. x /= y	(x = x / y)	Division
    $x = 10;
    $x /= 5;
    echo $x; // 2
    echo "<br>";

    //6. x %= y	 (x = x % y)	Modulus
    $x = 10;
    $x %= 3;
    echo $x; // 1
    echo "<br>";
    ?>

    <p>iii. Comparison operators</p>
    <?php
    // 1. == :- Returns true if $x is equal to $y
    $x = 100;
    $y = "100";
    var_dump($x == $y); //bool(true)
    echo "<br>";

    //2. === :-Returns true if $x is equal to $y, and they are of the same type
    var_dump($x === $y); //bool(false) ; types are not equal
    echo "<br>";

    //3.  !=  :-	Returns true if $x is not equal to $y
    var_dump($x != $y); //bool(false) because both are equal
    echo "<br>";

    //4. <>	(Not equal) :-  $y	Returns true if $x is not equal to $y.
    var_dump($x <> $y); // false because both are equal
    echo "<br>";

    // 5. !==	(Not identical):- Returns true if $x is not equal to $y, or they are not of the same type
    var_dump($x !== $y); // returns true because types are not equal
    echo "<br>";

    //6. >	(Greater than) :-Returns true if $x is greater than $y.
    var_dump($x > $y); //false 
    echo "<br>";

    //7. <	(Less than) :-Returns true if $x is less than $y.
    var_dump($x < $y); //false 
    echo "<br>";

    //8. >=	(Greater than or equal to) :-Returns true if $x is greater than or equal to $y.
    var_dump($x >= $y); //true because x=y.
    echo "<br>";

    //9. <=	(less than or equal to) :-Returns true if $x is less than or equal to $y.
    var_dump($x <= $y); //true  
    echo "<br>";

    //10. >	(Greater than) :-Returns true if $x is greater than $y.
    var_dump($x > $y); //false 
    echo "<br>";

    //11. <=>	(spaceship) :-Returns an integer less than, equal to, or greater than zero, depending on if $x is less than, equal to, or greater than $y. Introduced in PHP 7.
    $x = 5;
    $y = 10;
    echo ($x <=> $y); // return -1 because x is less than y.
    echo "<br>";

    $x = 10;
    $y = 10;
    echo ($x <=> $y); // return 0 because x is equal to y.
    echo "<br>";

    $x = 20;
    $y = 10;
    echo ($x <=> $y); // return 1 because x is greater than y.
    echo "<br>";

    ?>


    <p>iv. Increment/Decrement operators</p>
    <?php
    //1. ++$x	(Pre-increment) :-	Increments $x by one, then returns $x.
    $x = 10;
    echo (++$x); //11
    echo "<br>";

    //2. $x++	(post-increment) :-	Returns $x, then increments $x by one.
    $x = 10;
    echo ($x++); //10
    echo "<br>";
    echo ($x); // 11
    echo "<br>";

    //3. --$x	(Pre-decrement) :-	decrement $x by one, then returns $x.
    $x = 10;
    echo (--$x); //9
    echo "<br>";

    //4. $x--	(post-decrement) :-	Returns $x, then decrements $x by one.
    $x = 10;
    echo ($x--); //10
    echo "<br>";
    echo ($x); // 9
    echo "<br>";
    ?>

    <p>v. Logical operators :- The PHP logical operators are used to combine conditional statements.</p>
    <?php
    //1. and :- True if both $x and $y are true
    $x = 100;
    $y = 50;
    if ($x == 100 and $y == 50) {
        echo ("hello world"); //hello world
    }
    echo "<br>";

    //2. or :-True if either $x or $y is true
    if ($x == 100 or $y == 80) {
        echo "Hello Rakesh!"; //Hello world!
        echo "<br>";
    }

    // 3. xor :- True if either $x or $y is true, but not both.
    if ($x == 100 xor $y == 80) {
        echo "this is xor!"; //this is xor!
        echo "<br>";
    }

    // 4. || (or) :- True if either $x or $y is true
    if ($x == 100 || $y == 80) {
        echo "this is or!"; // this is or!
        echo "<br>";
    }

    // 5. ! (not) :-True if $x is not true
    if ($x !== 90) {
        echo "this is not!"; // this is not!
        echo "<br>";
    }
    ?>
    <p>vi. String operators</p>
    <?php
    //1. concatenation(.):- Concatenation of $txt1 and $txt2
    $text1 = "hello Rakesh";
    $text2 = " welcome";
    echo $text1 . $text2;
    echo "<br>";

    //2. Concatenation assignment(.=) :- Appends $txt2 to $txt1.
    $txt1 = "Hello";
    $txt2 = " Php!";
    $txt1 .= $txt2;
    echo $txt1;

    ?>
    <p>vii. Array operators :- The PHP array operators are used to compare arrays.</p>
    <?php
    $x = array("a" => "red", "b" => "green");
    $y = array("c" => "blue", "d" => "yellow");

    //1. + ($x +$y):- union of $x and $y
    print_r($x + $y); //Array ( [a] => red [b] => green [c] => blue [d] => yellow )
    echo "<br>";

    //2. ==($x==$y):- Return true if $x and $y have the same key/value pairs.
    var_dump($x == $y); //false
    echo "<br>";

    //3. ===($x==$y):- Returns true if $x and $y have the same key/value pairs in the same order and of the same types.
    var_dump($x == $y); //false
    echo "<br>";

    //4. != ($x!=$y):- Returns true if $x is not equal to $y
    var_dump($x != $y); //true
    echo "<br>";

    //5. <>	(Inequality	$x <> $y) :-Returns true if $x is not equal to $y
    var_dump($x <> $y); //true
    echo "<br>";

    //6. !==	(Non-identity	$x !== $y):-Returns true if $x is not identical to $y.
    var_dump($x !== $y); //true
    echo "<br>";
    ?>
    <p>viii. Conditional assignment operators :-The PHP conditional assignment operators are used to set a value
        depending on conditions.</p>
    <?php
    //1. ?:(ternary):- Returns the value of $x.
    //The value of $x is expr2 if expr1 = TRUE.
    //The value of $x is expr3 if expr1 = FALSE	
    
    // if empty($user) = TRUE, set $status = "anonymous"
    echo $status = (empty($user)) ? "anonymous" : "logged in";
    echo ("<br>");

    $user = "John Doe";
    // if empty($user) = FALSE, set $status = "logged in"
    echo $status = (empty($user)) ? "anonymous" : "logged in";
    echo ("<br>");

    //2. ??(null coalescing) $x = expr1 ?? expr2 :-Returns the value of $x.
    // If expr1 does not exist, or is NULL, the value of $x is expr2.
    // The value of $x is expr1 if expr1 exists, and is not NULL.
    // Introduced in PHP 7
    
    // variable $user is the value of $_GET['user']
    // and 'anonymous' if it does not exist
    echo $user = $_GET["user"] ?? "anonymous";
    echo ("<br>");

    // variable $color is "red" if $color does not exist or is null
    echo $color = $color ?? "red";
    ?>

</body>

</html>
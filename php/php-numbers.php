<!Doctype html>
<html lang="en">

<head>
    <title>PHP Numbers</title>
</head>

<body>
    <?php
    //1. PHP Integers :-a number without any decimal part.
    $x = 5985;
    var_dump(is_int($x));
    echo "<br>";

    $x1 = 59.85;
    var_dump(is_int($x1));
    echo "<br>";

    //2. PHP Floats :- A float is a number with a decimal point or a number in exponential form.
    $x2 = 10.365;
    var_dump(is_float($x2));
    echo "<br>";

    // 3. PHP Infinity :-A numeric value that is larger than PHP_FLOAT_MAX is considered infinite.
    $x3 = 1.9e411;
    var_dump($x3);
    echo "<br>";

    //4. PHP NaN :- Not a Number.
    $x4 = acos(8);
    var_dump($x4);
    echo "<br>";

    //5. PHP Numerical Strings :-The PHP is_numeric() function can be used to find whether a variable is numeric. The function returns true if the variable is a number or a numeric string, false otherwise.
    
    $x5 = 5987;
    var_dump(is_numeric($x5));
    echo "<br>";

    $x6 = "5987";
    var_dump(is_numeric($x6));
    echo "<br>";

    $x7 = " 65.87";
    var_dump(is_numeric($x7));
    echo "<br>";

    $x8 = "hello";
    var_dump(is_numeric($x8));
    echo "<br>";

    //6. PHP Casting Strings and Floats to Integers:-Sometimes you need to cast a numerical value into another data type.
    
    //The (int), (integer), or intval() function are often used to convert a value to an integer.
    
    //eg:- cast float to an integer.
    $x = 23465.768;
    $int_cast = (int) $x;
    echo $int_cast;
    echo "<br>";

    //eg:- cast string to an int
    $x = "23465.768";
    $int_cast = (int) $x;
    echo $int_cast;


    ?>

</body>

</html>
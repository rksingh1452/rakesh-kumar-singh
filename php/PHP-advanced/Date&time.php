<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Date and Time</title>
</head>

<body>
    <h1>PHP Date and Time :-</h1>
    <p>The PHP date() function is used to format a date and/or a time.</p>

    <h3>1. The PHP Date() Function :-</h3>
    <p>The PHP date() function formats a timestamp to a more readable date and time.</p>
    <!-- syntax:-
            date(format,timestamp) 
    format:- Required. Specifies the format of the timestamp

    timestamp:- Optional. Specifies a timestamp. Default is the current date and time-->

    <h3>2. Get a Date :-</h3>
    <?php
    echo "Today is " . date("Y/m/d") . "<br>";
    // d - Represents the day of the month (01 to 31)
    // m - Represents a month (01 to 12)
    // Y - Represents a year (in four digits)
    // Other characters, like"/", ".", or "-" can also be inserted between the characters to add additional formatting.
    echo "Today is " . date("Y.m.d") . "<br>";
    echo "Today is " . date("Y-m-d") . "<br>";
    echo "Today is " . date("l") . "<br>";
    //l (lowercase 'L') - Represents the day of the week
    
    ?>
    <h3>3. PHP Tip - Automatic Copyright Year :-</h3>
    &copy;2010-
    <?php echo date("Y"); ?>
    <!-- op:- ©2010- 2023 -->

    <h3>4. Get a Time :-</h3>
    <!-- H - 24-hour format of an hour (00 to 23)
    h - 12-hour format of an hour with leading zeros (01 to 12)
    i - Minutes with leading zeros (00 to 59)
    s - Seconds with leading zeros (00 to 59)
    a - Lowercase Ante meridiem and Post meridiem (am or pm) -->
    <?php
    echo "The time is " . date("h:i:sa");
    // The time is 04:19:36pm
    ?>

    <h3>5. Get Your Time Zone :-</h3>
    <p>It helps to show the of a paticular location like you live in India but the time shown is America</p>
    <?php
    date_default_timezone_set("America/New_York");
    echo "The time of America is :" . date("h:i:sa") . "<br>";
    ?>

    <h3>6. Create a Date With mktime() :-</h3>
    <p>The PHP mktime() function returns the Unix timestamp for a date. The Unix timestamp contains the number of
        seconds between the Unix Epoch (January 1 1970 00:00:00 GMT) and the time specified.</p>
    <!-- Synatx :-
         mktime(hour, minute, second, month, day, year)-->
    <?php
    $d = mktime(11, 14, 54, 8, 12, 2014);
    echo "Created date is " . date("Y-m-d h:i:sa", $d);
    ?>

    <h3>7. Create a Date From a String With strtotime() :-</h3>
    <p>The PHP strtotime() function is used to convert a human readable date string into a Unix timestamp (the number of
        seconds since January 1 1970 00:00:00 GMT).</p>
    <!-- syntax :-
        strtotime(time, now) -->
    <?php
    $d = strtotime("10:30pm April 15 2014");
    echo "Created date is " . date("Y-m-d h:i:sa", $d) . "<br>";

    // PHP is quite clever about converting a string to a date, so you can put in various values:-
    $d = strtotime("tomorrow");
    echo date("Y-m-d h:i:sa", $d) . "<br>";

    $d = strtotime("next Saturday");
    echo date("Y-m-d h:i:sa", $d) . "<br>";

    $d = strtotime("+3 month");
    echo date("Y-m-d h:i:sa", $d) . "<br>";

    // note :-However, strtotime() is not perfect, so remember to check the strings you put in there.
    ?>

    <h3>8. outputs the dates for the next six Saturdays:-</h3>
    <?php
    $startdate = strtotime("Saturday");
    $enddate = strtotime("+6 weeks", $startdate);
    while ($startdate < $enddate) {
        echo date("M d", $startdate) . "<br>";
        $startdate = strtotime("+1 week", $startdate);
    }
    ?>

    <h3>9. outputs the number of days until 4th of July:-</h3>
    <?php
    $d1 = strtotime("July 04");
    $d2 = ceil(($d1 - time()) / 60 / 60 / 24);
    echo "There are " . $d2 . " days until 4th of July.";
    ?>
</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Include Files</title>
</head>

<body>
    <h1>PHP Include Files :-</h1>
    <p>i. The include (or require) statement takes all the text/code/markup that exists in the specified file and copies
        it into the file that uses the include statement.</p>

    <p>ii. Including files is very useful when you want to include the same PHP, HTML, or text on multiple pages of a
        website.</p>

    <h3>The include and require statements are identical, except upon failure:-</h3>
    <p>i. require will produce a fatal error (E_COMPILE_ERROR) and stop the script</p>

    <p>ii. include will only produce a warning (E_WARNING) and the script will continue</p>

    <!-- syntax :-
        include 'filename'
        or
        require 'filename' -->

    <h3>PHP include example 1:-</h3>
    <p>Some more text.</p>
    <?php
    include 'include-eg1.php';
    ?>

    <h3>Example 2 include menu file</h3>
    <p>div elemnet is used to style the menu using css</p>
    <div class="menu">
        <?php
        include 'include-eg2.php'
            ?>
    </div>

    <h3>Example 3 include a file with some variables defined</h3>
    <?php
    include 'include-eg3.php';
    echo "I have a $color $car";
    ?>

    <h3>PHP include vs. require :-</h3>
    <p>The require statement is also used to include a file into the PHP code.</p>

    <p>i. when a file is included with the include statement and PHP cannot find it, the script will continue to
        execute:-</p>
    <?php
    include 'nofileExists.php';
    echo "I have a $color $car";
    ?>

    <p>ii. If we do the same example using the require statement, the echo statement will not be executed because the
        script execution dies after the require statement returned a fatal error:-</p>
    <?php require 'noFileExists.php';
    echo "I have a $color $car."; ?>
</body>

</html>
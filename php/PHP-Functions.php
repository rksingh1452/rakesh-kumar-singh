<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Functions</title>
</head>

<body>
    <h3>PHP User Defined Functions</h3>
    <p>A function is a block of statements that can be used repeatedly in a program.</p>
    <!-- syntax:-
        function functionName() {
            code to be executed;
        }-->

    <?php
    function writemsg()
    {
        echo "hello Rakesh <br>";
    }
    writemsg(); //calling the function
    
    //2. Function argument:- Information can be passed to functions through arguments. An argument is just like a variable.Add more than one argument using comma(,) sepearted.
    
    function familyname($fname)
    { //fname is argument
        echo "$fname Kumar.<br>";
    }
    familyname("Rakesh");
    familyname("Vikash");
    familyname("Aditya");

    //3. function with two argument:-
    function family($fName, $year)
    {
        echo "$fName Singh, Born in $year <br>";
    }
    family("Vishal", "1996");
    family("Vikram", "1998");

    //3.  PHP is a Loosely Typed Language:-
    
    //i. In the example above, notice that we did not have to tell PHP which data type the variable is.
    
    //ii. PHP automatically associates a data type to the variable, depending on its value.
    
    //iii. In PHP 7, type declarations were added. This gives us an option to specify the expected data type when declaring a function, and by adding the strict declaration, it will throw a "Fatal Error" if the data type mismatches.
    
    //eg :- we try to send both a number and a string to the function without using strict:
    function addNumbers(int $a, int $b)
    {
        return $a + $b;
    }
    echo addNumbers(5, "5 days"); //op-10 with a notice 
    // since strict is NOT enabled "5 days" is changed to int(5), and it will return 10
    ?>



    <p>To specify strict we need to set declare(strict_types=1);. This must be on the very first line of the PHP file.
    </p>

    <?php declare(strict=1); //strict declaration
    
    function adding(int $a, int $b)
    {
        return $a + $b;
    }
    echo adding(5, "6 days");
    echo "<br>";
    // since strict is enabled and "5 days" is not an integer, an error will be thrown
    
    //4. PHP Default Argument Value:-
    //eg:- we call the function setHeight() without arguments it takes the default value as argument:
    function setHeight(int $minHeight = 50)
    {
        echo "the height is : $minHeight <br>";
    }
    setHeight(350); //the height is : 350
    setHeight(); // the height is : 50 (ie. default)
    

    //5. PHP Functions - Returning values:-To let a function return a value, use the return statement:-
    function Sum(int $x, int $y)
    {
        $z = $x + $y;
        return $z;
    }
    echo "5 + 7=" . Sum(5, 7) . "<br>";
    echo "10 + 43=" . Sum(10, 43) . "<br>";

    //6. PHP Return Type Declarations:-
    //To declare a type for the function return, add a colon ( : ) and the type right before the opening curly ( { )bracket when declaring the function.
    function Sum2(float $a, float $b): float //type declare
    {
        return $a + $b;
    }
    echo Sum2(1.2, 2.4) . "<br>"; // 3.6
    echo Sum2(4, 3) . "<br>"; //7
    
    //You can specify a different return type, than the argument types, but make sure the return is the correct type:
    function addNumber(float $a, float $b): int
    {
        return (int) ($a + $b);
    }
    echo addNumber(1.2, 5.2); //6
    ?>

    <h3>7. Passing Arguments by Reference:-</h3>
    <p>In PHP, arguments are usually passed by value, which means that a copy of the value is used in the function and
        the variable that was passed into the function cannot be changed.

        When a function argument is passed by reference, changes to the argument also change the variable that was
        passed in. To turn a function argument into a reference, the & operator is used:</p>
    <?php
    function add_five(&$value)
    {
        $value += 5;
    }
    $num = 2;
    add_five($num);
    echo $num;
    ?>
</body>

</html>
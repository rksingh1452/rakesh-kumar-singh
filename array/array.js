//1.  Creating an array:-
const car=["tata","xuv","harrier","thar"];
document.getElementById("cArr").innerHTML+="<br>"+car;

//Spaces and line breaks are not important. A declaration can span multiple lines:
const car1=["tata",
"xuv",
"harrier",
"thar"
];

document.getElementById("cArr").innerHTML+="<br>"+"<br>"+"Spaces and line breaks are not important. A declaration can span multiple lines:-"+"<br>"+ car1;

// you can also create an array and then provide the elements:-
const car2=[];
car2[0]= "tata";
car2[1]="xuv";
car2[2]="harrier";
car2[3]="thar";

document.getElementById("cArr").innerHTML+="<br>"+"<br>"+"you can also create an array and then provide the elements:-"+"<br>"+ car2;

//creating a array using keyword new:-
const car3=new Array("tata","xuv","harrier","thar");
document.getElementById("cArr1").innerHTML+="<br>"+car3;

// # 2.Accessing Array Elements:-
const car4=["tata","xuv","harrier","thar"];
let x=car4[0]; //index starts with zero;
let x1=car4[1];
let x2=car4[3];
document.getElementById("aArr").innerHTML+="<br>"+
"accessing element at 0  car4[0] ="+x +"<br>" +
"accessing element at 1  car4[1] =" + x1 +"<br>" +
"accessing element at 3  car4[3] ="   +x2;

// changing an array element:-
document.getElementById("aArr").innerHTML+="<br>"+"<br>"+"Original Array"+"<br>"+car4;
car4[0]="xuv700";
document.getElementById("aArr").innerHTML+="<br>"+"<br>"+"changing array element :-"+"<br>"+car4;

// Converting an array to a string:-
const car5=["tata","xuv","harrier","thar"];
let x3= car5.toString();
console.log(x3, typeof x3);

// Arrays are object:-
console.log(typeof car5); // op- object


// Accessing 1st and last element of an array:-
const car6=["tata","xuv","harrier","thar"];
let x4= car6[0];                // op- tata
let x5=car5[car6.length-1];     //op- thar
console.log(x4, x5);

//# looping an array element:-
// 1. for loop
const fruit=["mango","apple","banana","litchi"];
let text="<ol>"
for (let i=0; i<fruit.length ;i++){
    text+= "<li>"+fruit[i] +"<br>" ;
}
console.log(text);
document.getElementById("loop").innerHTML+="<br>"+"for loop:-"+text;

//2.for each:-
const fruit1=["mango","apple","banana","litchi"];
let text1="<ul>";
fruit1.forEach(myfunction);
function myfunction(value){
    text1+="<li>"+value +"</li>";
}
document.getElementById("loop").innerHTML+="<br>"+"forEach loop:-"+text1;

//  #Adding Array Element:-

//1. push():-adding element to the last.
const fruit2=["mango","apple","banana","litchi"];
fruit2.push("lemon");
console.log(fruit2);

//2. length property:-
fruit2[fruit2.length]="papaya";
console.log(fruit2);

//note:- adding elements to high indexes can create undefine hole in an array:-
fruit2[10]="grapes";    
console.log(fruit2);

//If you use a named index when accessing an array, JavaScript will redefine the array to a standard object, and some array methods and properties will produce undefined or incorrect results.
const person = [];
person["firstName"] = "John";
person["lastName"] = "Doe";
person["age"] = 46; 

console.log(person.length); //op-0;

//How to Recognize an Array:-
let t= typeof fruit;
document.getElementById("RcArr").innerHTML+="<br>"+"types of fruit array:- "+ t;

//sol. 1:- returns true if array or false.
let t1 = Array.isArray(fruit);
document.getElementById("RcArr").innerHTML+="<br>"+"using Array.isArray():- "+ t1;

//2. instance of
t2=fruit instanceof Array;
document.getElementById("RcArr").innerHTML+="<br>"+"using fruits instanceof Array:- "+ t2;









